

- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)



> 2019/4/17 17:55



承蒙各位的关爱和支持，自 [**Python 爬虫与文本分析现场班**](https://gitee.com/arlionn/Course/blob/master/Python_text.md) (2019 年 5 月，太原)，[**空间计量专题现场班**](https://gitee.com/arlionn/Course/blob/master/Spatial.md) (2019 年 6 月，西安)， 以及 [**连享会 - 诚邀 2019 现场班助教**](https://gitee.com/arlionn/Course/blob/master/%E5%8A%A9%E6%95%99%E6%8B%9B%E8%81%98/%E8%BF%9E%E4%BA%AB%E4%BC%9A-%E8%AF%9A%E9%82%80%202019%20%E7%8E%B0%E5%9C%BA%E7%8F%AD%E5%8A%A9%E6%95%99.md)  发布以来，我们陆续接到了来自 **20 余所** 高校 **30 余位** 老师和同学的申请。

非常感谢大家对连享会现场培训的关注和支持。

经过主办方和任课老师的遴选，最终确定如下 6 位申请者担任本次培训的助教：

> **Python 爬虫与文本分析现场班：**
>  &emsp; &emsp; 张晓明(中国人民大学)，吴玉轩(南开大学)，郭李鹏(晋城市财政局)
>
> **空间计量专题现场班：**
>  &emsp; &emsp; 韩少真(西北大学)，崔娜(西安交通大学)，丁海(华中科技大学)

**温馨提示：** 请各位入选的老师和同学收到邮件通知或看到此通知后，发送邮件到 [StataChina@163.com](StataChina@163.com)，确认能否参加此次培训，以便我们及时做出调整，增加新的助教名额。     
邮件要求如下：
- **邮件标题：** Stata培训助教确认-姓名-学校
- **邮件内容：** (1) 确认是否能参加此次培训；(2) 填写您的联系方式 (姓名；手机号；邮箱；微信号)。

**助教遴选的基本规则如下：**
(1) 专业或研究背景与本次培训的内容之间的相关度；
(2) 有独立进行实证分析的经验；

对于未能入选担任本次培训助教的各位老师和学生，我们也详细记录了各位的资料，希望在日后的培训中可以邀请各位担任与您知识背景相关的专题的助教工作。

>秉承**「知识需要分享」**的理念，我们也诚挚邀请各位将研究和学习过程中的经验和心得记录下来，写成具有较强可读性和实操性的推文，通过 [简书平台](http://www.jianshu.com/) 投稿到 [Stata连享会-简书](http://www.jianshu.com/u/69a30474ef33) (投稿方法：在简书平台中写好推文，点击页面右下角【**文章投稿**】图标；搜索【**Stata连享会**】；点击投稿)；或通过邮件发送到 [StataChina@163.com](StataChina@163.com)。     
在日后的助教遴选中，我们会优先邀请有投稿经历的老师和同学。

衷心感谢各位的支持和关注！

**特别说明：** 文中包含的链接在微信中无法生效。请点击本文底部左下角的【阅读原文】。



>#### 关于我们

- 【**Stata 连享会(公众号：StataChina)**】由中山大学连玉君老师团队创办，旨在定期与大家分享 Stata 应用的各种经验和技巧。
- 公众号推文同步发布于 [CSDN-Stata连享会](https://blog.csdn.net/arlionn) 、[简书-Stata连享会](http://www.jianshu.com/u/69a30474ef33) 和 [知乎-连玉君Stata专栏](https://www.zhihu.com/people/arlionn)。可以在上述网站中搜索关键词`Stata`或`Stata连享会`后关注我们。
- 点击推文底部【阅读原文】可以查看推文中的链接并下载相关资料。
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

>#### 联系我们

- **欢迎赐稿：** 欢迎将您的文章或笔记投稿至`Stata连享会(公众号: StataChina)`，我们会保留您的署名；录用稿件达`五篇`以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **意见和资料：** 欢迎您的宝贵意见，您也可以来信索取推文中提及的程序和数据。
- **招募英才：** 欢迎加入我们的团队，一起学习 Stata。合作编辑或撰写稿件五篇以上，即可**免费**获得 Stata 现场培训 (初级或高级选其一) 资格。
- **联系邮件：** StataChina@163.com

>#### 往期精彩推文
- [Stata连享会推文列表](https://www.jianshu.com/p/de82fdc2c18a) 
- Stata连享会 [精品专题](https://gitee.com/arlionn/stata_training/blob/master/README.md)  || [精彩推文](https://github.com/arlionn/stata/blob/master/README.md)

---
![欢迎加入Stata连享会(公众号: StataChina)](http://upload-images.jianshu.io/upload_images/7692714-5d8bd5ce81ba701c.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240 "扫码关注 Stata 连享会")
