### 连玉君的profile.do文档 [下载](https://gitee.com/arlionn/stata_training/blob/master/dofile-data/profile.do)。

- 你的profile.do文档存放于stata安装目录下，可以使用doedit D:\stata15\profile.do打开并编辑。
- 如果没有的话，可以前往如下地址 [下载](https://gitee.com/arlionn/stata_training/blob/master/dofile-data/profile.do) 
一份放置于 `D:\stata15` 目录下，根据需要酌情修改。重启 `Stata` 后，`profile.do` 中的设定方可生效。
- 我提供的 `profile.do` 文档可以供大家参考，亦可直接复制后使用。